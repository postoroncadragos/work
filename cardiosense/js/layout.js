function setContainersEqual() {
    var videoHeight = document.getElementById('video-dots-overly').offsetHeight;
    var contentHeight = document.getElementById('home-main-section').offsetHeight;

    if (videoHeight > contentHeight + 72) {
        document.getElementById("home-main-section").style.height = videoHeight - 72 + "px";
    } else {
        document.getElementById("video-dots-overly").style.height = contentHeight + 72 + "px";
    }
}

setContainersEqual()
window.addEventListener('resize', setContainersEqual);
